import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GreeniaresComponent } from './greeniares.component';

describe('GreeniaresComponent', () => {
  let component: GreeniaresComponent;
  let fixture: ComponentFixture<GreeniaresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GreeniaresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GreeniaresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
