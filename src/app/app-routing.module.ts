import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './auth.guard';
import { RegistrationComponent } from './registration/registration.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { ExampleComponent } from './example/example.component';
import {  Example22Component} from './example22/example22.component'
const routes: Routes = [
  
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'forgotpassword',
    component: ForgotpasswordComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path:'registration',
    component: RegistrationComponent
  },
  
  {
    path: 'example',
    component: ExampleComponent
  },
  {
    path: 'example22',
    component:  Example22Component
  }

];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
