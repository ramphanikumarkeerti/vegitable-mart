import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  event_list = [
    {
      event:' Event 1',
      eventLocation:'Bangalore',
      eventDescription:'In bangalore, first event is going to happen. Please be careful about it',
      img: 'https://picsum.photos/900/500?random&t=1',
      eventStartDate: new Date('2019/05/20'),
      eventEndingDate: new Date('2019/05/24')
    },
     {
      event:' Event 2',
      eventLocation:'Dubai',
      eventDescription:'Dubai is another place to host so,e, first event is going to happen. Please be careful about it',
      img: 'https://picsum.photos/900/500?random&t=3',
      eventStartDate: new Date('2019/07/28'),
      eventEndingDate: new Date('2019/07/30')
    },
     {
      event:' Event 3',
      eventLocation:'New York',
      eventDescription:'NewYork sits on top of event hosting',
      img: 'https://picsum.photos/900/500?random&t=4',
      eventStartDate: new Date('2020/05/20'),
      eventEndingDate: new Date('2020/05/24')
    },
     {
      event:' Event 4',
      eventLocation:'Singapore',
      eventDescription:'Singapore is another great hosting city',
      img: 'https://picsum.photos/900/500?random&t=6',
      eventStartDate: new Date('2018/05/20'),
      eventEndingDate: new Date('2018/05/24')
    },
    {
      event:' Event 5',
      eventLocation:'Berlin',
      eventDescription: 'Berlin is best place to hang on',
      img: 'https://picsum.photos/900/500?random&t=7',
      eventStartDate: new Date('2017/07/10'),
      eventEndingDate: new Date('2017/08/14')
    },
    {
      event:' Event 6',
      eventLocation:'Mumbai',
      eventDescription:'Mumbai is hub of startups',
      img: 'https://picsum.photos/900/500?random&t=8',
      eventStartDate: new Date(),
      eventEndingDate: new Date()
    },
    {
      event:' Event 7',
      eventLocation:'Barcelona',
      eventDescription:'Barcelona is another good city',
      img: 'https://picsum.photos/900/500?random&t=6',
      eventStartDate: new Date(),
      eventEndingDate: new Date()
    },
  ]


  upcoming_events =  this.event_list.filter( event => event.eventStartDate > new Date());
  past_events = this.event_list.filter(event => event.eventEndingDate < new Date());
  current_events =  this.event_list.filter( event => (event.eventStartDate >= new Date() && (event.eventEndingDate <= new Date())))
  itemList: any =[];
  imagesshow: boolean =false;
  subItemshow: any ;
  item: any;
  pushData: any = [];
  constructor() { }

  ngOnInit(): void {
    this.itemList =[{ "catName":"Fruits",
                     "catIamge":"../../assets/fresh-fruits.jpg",
                     "itemCost":100,
                     "subcat":[{"name":"Apple",
                                "Iamge":"../../assets/apple.jpg",
                                "qty":1,"pirce":100},
                                {"name":"Orange",
                                "Iamge":"../../assets/orange.jpg",
                                "qty":1,"pirce":100}]},
                      { "catName":"Vegitables",
                        "catIamge":"../../assets/fruits-vegetables.jpg",
                        "itemCost":150,
                         "subcat":[{"name":"Onions",
                                    "Iamge":"../../assets/Red-Onion.jpg",
                                     "qty":1,"pirce":150},
                                   {"name":"Tamoto",
                                    "Iamge":"../../assets/Tomato_je.jpg",
                                    "qty":1,"pirce":150}]},
                      //  { "catName":"Grocery",
                      //    "catIamge":"../../assets/grocery.png",
                      //     "itemCost":120,
                      //     "subcat":[{"name":"Kazu",
                      //                "Iamge":"../../assets/Badham.jpg",
                      //                 "qty":1,"pirce":120},
                      //                {"name":"Bhadum",
                      //                 "Iamge":"../../assets/Badham.jpg",
                      //                 "qty":1,"pirce":120}]}        
                                       ] 
    console.log(this.itemList);                            
  }
  getItems(item:any){
    
    this.subItemshow = item;
    console.log(this.subItemshow);
    this.imagesshow = true

  }
  minus(value:any){
   console.log(value)
   
   if(value.qty == "1"){
  }else{
    value.qty -= 1;
    value.itempirce =value.qty * value.pirce;
  console.log(value);
  this.item = value;
  //this.total = Number(this.result[0].cost * qty.qty);
  //this.price = true;
  //this.priceShow = false;
  //qty.totalItemCost = this.total;
//this.item.weight = qty.qty * this.item.calWeight
  }
  }
  plus(value:any){
    console.log(value)
    
    value.qty += 1;
    value.itempirce =value.qty * value.pirce;
    this.item = value;
  
  }
  addCart(item:any){
    console.log(item);
    this.pushData.push(item);
    console.log(this.pushData);
  

  }
  deleteItem(data:any){
    const index: number = this.pushData.indexOf(data);
    this.pushData.splice(index, 1);

  }
}
