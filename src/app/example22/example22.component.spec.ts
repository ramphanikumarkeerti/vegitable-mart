import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Example22Component } from './example22.component';

describe('Example22Component', () => {
  let component: Example22Component;
  let fixture: ComponentFixture<Example22Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Example22Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Example22Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
